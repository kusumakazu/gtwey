/**
 * View Models used by Spring MVC REST controllers.
 */
package co.id.kusumakazu.web.rest.vm;
